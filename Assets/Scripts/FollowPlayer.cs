﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    // Transform determina la Position, Rotation, y Scale de cada objeto en la escena
    public Transform player;
    public Vector3 offset;//almacena 3 numeros en X(centrado) Y(arriba-abajo) Z(adelante-atras) 
    
	// Update is called once per frame
	void Update () {

        //Debug.Log(player.position); //indica posicion del objeto

        //con minus se refiere al transform de nuestro objeto actual
        transform.position = player.position + offset; //posicion de nuestra camara a la de nuestro objeto
        //La camara sigue al objeto pero parece como si estuviera en primera persona
        //Para situar la cama mas atras creamos otra variable 
	}
}
