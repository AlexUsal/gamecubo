﻿using UnityEngine;

public class Trigger : MonoBehaviour {

    public GameManager gameManager;

    //Para detectar que algo ha chocado con el cubo
    //Se llama a OnTriggerEnter cuando GameObject colisiona con otro GameObject.
    void OnTriggerEnter()
    {
        gameManager.completeLevel();
    }
}
