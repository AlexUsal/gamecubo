﻿using UnityEngine;

public class playerMovement : MonoBehaviour
{
    //Los Rigidbodies le permite a sus GameObjects actuar bajo el control de la física. El Rigidbody puede recibir fuerza y 
    //torque para hacer que sus objetos se muevan en una manera realista. Cualquier GameObject debe contener un Rigidbody 
    //para ser influenciado por gravedad, 
    //actué debajo fuerzas agregadas vía scripting, o interactuar con otros objetos a través del motor de física NVIDIA Physx.
    public Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb.useGravity = true;
    }

    public float forwardForce = 1500f;
    public float sidewaysForce = 700f;

    //Drag se puede utilizar para ralentizar un objeto. Cuanto mayor sea el arrastre, más se ralentiza el objeto.

    void FixedUpdate() {

            rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        if (Input.GetMouseButtonDown(0))
        {   //gira a la izq
            rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0,ForceMode.VelocityChange); //Aplique el cambio de velo instantáneamente 

        }

        if (Input.GetMouseButtonDown(1))
        {
            rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }

        //Para reiniciar el juego cuando el player se caiga pero esto se repite muchas veces y solo queremos que sea una 
        if(rb.position.y < -1f)
        {
            FindObjectOfType<GameManager>().getTexto();
        }


    }

}
