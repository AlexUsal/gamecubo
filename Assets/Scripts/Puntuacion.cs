﻿using UnityEngine;
using UnityEngine.UI;

public class Puntuacion : MonoBehaviour {

    //Referencia a nuestro player ya asi con la posición y poder usar eso ya que la distancia recorrida es la misma
    //Se hace a Transform ya que es el componente encargado de la posición.
    public Transform player;
    //referencia al componente text
    public Text puntuacion;

    bool flag = false;
	
	// Update is called once per frame
	void Update () {

        if (flag == false)
        {
            puntuacion.text = player.position.z.ToString("0");
        }
        else
        {
            puntuacion.text = "Game Over";
        }

        
	}

    public void setFlag()
    {
        this.flag=true;
    }

}
