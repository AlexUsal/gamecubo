﻿using UnityEngine;

public class playerColision : MonoBehaviour {


    public playerMovement movement; // Referencia al script y arrastramos en unity su referencia
    //public GameManager gameManager;

    //Recuerda de primeras ya choca con el suelo
    void OnCollisionEnter(Collision info) //Informacion de la colision
    {
        //Debug.Log(info.collider.name);

        //if (info.collider.name == "Esfera") //esta llamada no se deberia usar ya que en un juego puedo haber millones de objetos
            if (info.collider.tag == "obstaculo") //afectando al rendimiento hay que agregar un Tag y llamarlo
        {
            Debug.Log("choco con esfera");
            //movement.enabled = false;
            GetComponent<playerMovement>().enabled = false;// esto es igual a lo de arriba

            //Esto devuelve el Objeto que coincide con el tipo especificado. 
            FindObjectOfType<GameManager>().getTexto();
        }
    }

}
