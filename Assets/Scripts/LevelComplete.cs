﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour
{

    public void loadNextLevel()
    {
        //Cargamos la escena que queremos mostrar, en este caso la siguiente a la actual
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
