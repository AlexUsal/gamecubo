﻿using UnityEngine;
using UnityEngine.SceneManagement; //Para cambiar a una escena diferente  o recargar la escena en la que estamos

public class GameManager : MonoBehaviour {

    string textoFin = "Game Over";
    bool fin = false;
    //Referencia a una interfaz de usuario
    public GameObject completeLevelUI;

    public void getTexto()
    {
        if (fin == false)
        {
            fin = true;
            Debug.Log(this.textoFin);
            FindObjectOfType<Puntuacion>().setFlag();
            //restart();
            Invoke("restart",2f); //Tarda 2f en llamar al metodo restart()
        }
    }


    public void completeLevel()
    {
        completeLevelUI.SetActive(true); //lo asociamos con nuestro panel LevelComplete creado antes
    }


    void restart()
    {
        //SceneManager.LoadScene("Scene1");
        //SceneManager.GetActiveScene().name() -> devuelve el nombre de la escena actual
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
